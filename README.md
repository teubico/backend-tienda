Please move env_file.example to env_file and make changes accordingly.

Build this container

    docker build . -t teubico:latest

Then run this container with the following command:

    docker run --env-file config/env_file -v $PWD/src:/var/www -v $PWD/sockets:/var/sockets:rw -v $PWD/logs:/var/log/teubico:rw -d teubico:latest
