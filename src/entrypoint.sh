#!/bin/bash

PIDFILE=/var/sockets/uwsgi.pid

function stop_process() {
  if [ -f $PIDFILE ]
	then
  	kill -s SIGQUIT `cat $PIDFILE`
		rm $PIDFILE
	fi
}

function start_uwsgi() {
  # Apply migrations to the latest version
	alembic upgrade head
  # Start server
  uwsgi --enable-threads --pidfile $PIDFILE --uid $RUNNER_UID --gid $RUNNER_GID --logto2 /var/log/teubico/uwsgi-teubico.log  --ini uwsgi.ini
}

trap stop_process SIGTERM

start_uwsgi
