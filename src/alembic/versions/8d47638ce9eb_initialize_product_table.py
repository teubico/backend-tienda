"""initialize product table

Revision ID: 8d47638ce9eb
Revises:
Create Date: 2020-12-10 14:34:43.584669

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8d47638ce9eb'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
      'products_v2',
      sa.Column(
        'id',
        sa.BigInteger(),
        autoincrement=True,
        primary_key=True
      ),
      sa.Column('mgf_code', sa.Unicode(100)),
      sa.Column('provider', sa.Unicode(50)),
      sa.Column('name', sa.Unicode(50), nullable=False),
      sa.Column('description', sa.Unicode(2000)),
      sa.Column('updated', sa.DateTime()),
      sa.Column('stock', sa.Integer(), nullable=False),
      sa.Column('price', sa.Numeric(precision=10,scale=2)),
      sa.Column('discount', sa.Float()),
      sa.Column('location', sa.Integer()),
      sa.Column('image', sa.String(200))
    )


def downgrade():
    op.drop_table('products_v2')
