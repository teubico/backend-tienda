from flask import render_template
import connexion
from app.models import get_engine

# Create the application instance
app = connexion.App(__name__, specification_dir='swagger/')

# Read the swagger.yml file to configure the endpoints
app.add_api('tienda.yml')

#initialize the database
app.db_engine = get_engine()

# Create a URL route in our application for "/"
@app.route('/api')
def home():
    """
    This function just responds to the browser ULR
    localhost:5000/
    :return:        the rendered template 'home.html'
    """
    return render_template('home.html')
