from __future__ import print_function
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app import app
from app.models import *
import decimal

def get_new():

    Session = sessionmaker()
    Session.configure(bind=app.db_engine)
    session = Session()

    products = session.query(Product).order_by(Product.id.desc()).limit(6).all()
    result = []
    for product in products:
      result.append(
          {
            'id': product.id,
            'sku': product.mgf_code,
            'name': product.name,
            'current_stock': product.stock,
            'current_price': product.price,
            'discount_price': (product.price - product.price*decimal.Decimal(product.discount)),
            'location': product.location,
            'img': 'https://tienda.teubi.co/images/default_img.jpg' if product.image is None else product.image
          }
        )

    session.close()

    return { 'products': result }

def get_all():

    Session = sessionmaker()
    Session.configure(bind=app.db_engine)
    session = Session()

    products = session.query(Product).order_by(Product.id.desc()).all()
    result = []
    for product in products:
      result.append(
          {
            'id': product.id,
            'sku': product.mgf_code,
            'name': product.name,
            'current_stock': product.stock,
            'current_price': product.price,
            'discount_price': (product.price - product.price*decimal.Decimal(product.discount)),
            'location': product.location,
            'img': 'https://tienda.teubi.co/images/default_img.jpg' if product.image is None else product.image
         }
        )

    session.close()

    return { 'products': result }
