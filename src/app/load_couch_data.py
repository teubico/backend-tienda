from __future__ import print_function
from datetime import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from cloudant.client import Cloudant
from app.local_config import *

def idx_exists(datalist, index):
  result = False
  dummy = None
  try:
    dummy = datalist[index]
    if dummy is not None and dummy is not "":
      result = True
  except:
    result = False
  return result

def process_social_networks(row):
  result = list()
  
  if idx_exists(row,15):
    result.append({
        "name" : "Facebook",
        "handler" : row[15]
      })
  if idx_exists(row,16):
    handler = row[16].replace("@","") if row[16].startswith("@") else row[16]
    result.append({
        "name" : "Instagram",
        "handler" : "https://www.instagram.com/"+row[16]
      })
  if idx_exists(row,17):
    handler = row[17].replace("@","") if row[17].startswith("@") else row[17]
    result.append({
        "name" : "Twitter",
        "handler" : "https://twitter.com/"+row[17]
      })
  if idx_exists(row,18):
    result.append({
        "name" : "LinkedIn",
        "handler" : row[18]
      })
  if idx_exists(row,19):
    result.append({
        "name" : "YouTube",
        "handler" : row[19]
      })
  if idx_exists(row,20):
    result.append({
        "name" : "Twitch",
        "handler" : row[20]
      })
  if idx_exists(row,21):
    result.append({
        "name" : "Other",
        "handler" : row[21]
      })
  if idx_exists(row,22):
    result.append({
        "name" : "Other",
        "handler" : row[22]
      })
  if idx_exists(row,23):
    result.append({
        "name" : "Other",
        "handler" : row[23]
      })
  if idx_exists(row,24):
    result.append({
        "name" : "Other",
        "handler" : row[24]
      })
  
  return result

# Create a handler for our read (GET) people
def get():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('./token.pickle'):
        with open('./token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                './credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('./token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=SAMPLE_RANGE_NAME).execute()
    values = result.get('values', [])

    response = list()
    client = Cloudant(
        COUCH_USER,
        COUCH_PASS,
        url=COUCH_URL,
        connect=True
        )
    lomasverdesdb = client['lomasverdes']

    if not values:
      print('No data found.')
    else:
      for row in values:
        if idx_exists(row,1):
          data = {
                  "id": row[0],
                  "name": row[3],
                  "description": row[4],
                  "keywords": list(map(lambda item: item.replace("#","") if item.startswith("#") else item, row[6].split(" "))),
                  "address": row[7],
                  "geolocation": {
                    "lat": float(row[13]) if idx_exists(row,13) else 13.70670,
                    "lon": float(row[14]) if idx_exists(row,14) else -89.24926
                    },
                  "phone": row[8],
                  "website": row[5],
                  "social_networks": process_social_networks(row)
              }
          lomasverdesdb.create_document(data)
          print("Data added")

get()
