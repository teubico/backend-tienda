from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa
import os

Base = declarative_base()

class Product(Base):
    __tablename__ = 'products_v2'

    id = sa.Column(
        sa.BigInteger(),
        autoincrement=True,
        primary_key=True
      )

    mgf_code = sa.Column(sa.Unicode(100))
    provider = sa.Column(sa.Unicode(50))
    name = sa.Column(sa.Unicode(50), nullable=False)
    description = sa.Column(sa.Unicode(2000))
    updated = sa.Column(sa.DateTime())
    stock = sa.Column(sa.Integer(), nullable=False)
    price = sa.Column(sa.Numeric(precision=10,scale=2))
    discount = sa.Column(sa.Float())
    location = sa.Column(sa.Unicode(10))
    image = sa.Column(sa.String(200))

def get_engine():
    # Build configuration from environment
    db_driver = os.environ['DB_DRIVER']
    db_user = os.environ['DB_USER']
    db_pass = os.environ['DB_PASS']
    db_host = os.environ['DB_HOST']
    db_name = os.environ['DB_INSTANCE']

    url = "{}://{}:{}@{}/{}".format(
      db_driver,
      db_user,
      db_pass,
      db_host,
      db_name
    )
    return sa.create_engine(url, encoding='UTF-8', echo=True)
