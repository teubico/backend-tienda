FROM debian:buster-slim

# Fix timezone requirements
ENV TZ=America/El_Salvador
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir -p /var/www
WORKDIR /var/www

VOLUME /var/www/

RUN apt-get update && apt-get -y install bash uwsgi uwsgi-plugin-python3 python3 python3-pip default-libmysqlclient-dev gcc sudo

COPY src/requirements.txt .

RUN pip3 install -r requirements.txt && rm requirements.txt

RUN mkdir -p /var/sockets && mkdir -p /var/log/teubico

VOLUME /var/sockets
VOLUME /var/log/teubico

ENV RUNNER_UID www-data
ENV RUNNER_GID www-data

CMD ["/var/www/entrypoint.sh"]
